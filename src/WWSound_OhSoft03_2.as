package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft03_2.swf",symbol="WWSound_OhSoft03_2")]
	
	public dynamic class WWSound_OhSoft03_2 extends Sound {
		
		public function WWSound_OhSoft03_2() {
			super();
		}
	}

}
