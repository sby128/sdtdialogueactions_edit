package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft01.swf",symbol="WWSound_OhSoft01")]
	
	public dynamic class WWSound_OhSoft01 extends Sound {
		
		public function WWSound_OhSoft01() {
			super();
		}
	}

}
