package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard05_2.swf",symbol="WWSound_AhHard05_2")]
	
	public dynamic class WWSound_AhHard05_2 extends Sound {
		
		public function WWSound_AhHard05_2() {
			super();
		}
	}

}
