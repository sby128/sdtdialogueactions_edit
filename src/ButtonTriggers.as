﻿package {
	public class ButtonTriggers extends CustomTriggerHolder {

		public function ButtonTriggers(G:Object, M:Main) {
			super(G,M);
		}

		public function registerTriggers(t:TriggerManager):void {
			for (var i:uint = 1; i <= 10; i++) {
				t.registerTrigger("BUTTON"+i+"_ON",0,new FunctionObject(this.button_on,this,[i]));
				t.registerTrigger("BUTTON"+i+"_OFF",0,new FunctionObject(this.button_off,this,[i]));
			}
			t.registerTrigger("BUTTONALL_ON",0,new FunctionObject(this.buttonAll_on,this,[]));
			t.registerTrigger("BUTTONALL_OFF",0,new FunctionObject(this.buttonAll_off,this,[]));
			t.registerTrigger("BUTTONALL_CLEAR",0,new FunctionObject(this.buttonAll_clear,this,[]));
		}

		public function registerVariables(v:VariableManager):void {
			for (var i:uint = 1; i <= 10; i++) {
				v.registerVariableWrite("da.button"+i+".name",new FunctionObject(this.setButtonName,this,[i]));
				v.registerVariableWrite("da.button"+i+".varname",new FunctionObject(this.setButtonVarName,this,[i]));
			}
		}
		
		public function button_on(... args):void {
			m.wwButtonArray[args[0]-1].visible = true;
		}

		public function button_off(... args):void {
			m.wwButtonArray[args[0]-1].visible = false;
		}
		
		public function setButtonName(index:uint, buttonName:String):void {
			m.wwButtonArray[index - 1].buttonText.text = buttonName;
		}
		
		public function setButtonVarName(index:uint, varName:String):void {
			m.wwButtonArray[index - 1].buttonText.text = m.getVariableManager().getVariableValueViaSDT(varName);
		}
		
		// Only turn on buttons that have non-null names
		public function buttonAll_on(... args):void {
		    for (var i:uint = 0; i < m.wwButtonArray.length; i++) {
				if (m.wwButtonArray[i].buttonText.text != "") {
			        m.wwButtonArray[i].visible = true;
				}
			}
		}

		// Hide every button
		public function buttonAll_off(... args):void {
		    for (var i:uint = 0; i < m.wwButtonArray.length; i++) { 
			    m.wwButtonArray[i].visible = false;
			}
		}
		
		// Hide every button and clear their names
		public function buttonAll_clear(... args):void {
		    for (var i:uint = 0; i < m.wwButtonArray.length; i++) { 
			    m.wwButtonArray[i].visible = false;
				m.wwButtonArray[i].buttonText.text = "";
			}
		}
	}
}