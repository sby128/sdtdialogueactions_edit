package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class FrameDelayedEvent extends Sprite
	{
		private var delay:int = 0;
		private var check:FunctionObject;
		private var endTrigger:FunctionObject;
		private var done:Boolean = false;
		public function FrameDelayedEvent(Delay:int, progressCheck:FunctionObject, trigger:FunctionObject) 
		{
			delay = Delay;
			check = progressCheck;
			endTrigger = trigger;
			if (!stage) {
				this.addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			} else {
				addedToStage();
			}
		}
		
		public function addedToStage(e:Event = null) {
			this.removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			this.addEventListener(Event.REMOVED_FROM_STAGE, wtf);
			this.addEventListener(Event.ENTER_FRAME, enterFrame);
		}
		
		public function enterFrame(e:Event = null) {
			if (check.call()) {
				delay--;
			}
			if (delay <= 0 && !done) {
				fireEndTrigger();
			}
		}
		private function fireEndTrigger():void {
			this.removeEventListener(Event.ENTER_FRAME, enterFrame);
			endTrigger.call();
			done = true;
			this.parent.removeChild(this);
		}
		
		public function wtf(e:Event = null) {
			if (!done) {
				//wtf
			}
			this.removeEventListener(Event.REMOVED_FROM_STAGE, wtf);
		}
		
	}

}