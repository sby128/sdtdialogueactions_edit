package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_Ow02.swf",symbol="WWSound_Ow02")]
	
	public dynamic class WWSound_Ow02 extends Sound {
		
		public function WWSound_Ow02() {
			super();
		}
	}

}
