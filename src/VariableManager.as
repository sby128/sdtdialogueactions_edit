package {
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableManager {
		private var g:Object;
		private var globalVariables:Array;
		private var registeredVariablesSet:Dictionary;
		private var registeredVariablesGet:Dictionary;
		private var writeListeners:Array;
		
		public function VariableManager(G:Object) {
			g = G;
			registeredVariablesGet = new Dictionary();
			registeredVariablesSet = new Dictionary();
			globalVariables = new Array();
			writeListeners = new Array();
		}
		
		public function registerVariableWriteHandlerComms(name:String, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array):void {
			registerVariableWrite(name, new FunctionObject(callbackFunction, callbackTarget, callbackArgs));
		}
		
		public function registerVariableWriteListenerComms(name:String, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array):void {
			registerVariableWriteListener(name, new FunctionObject(callbackFunction, callbackTarget, callbackArgs));
		}
		
		public function registerVariableReadHandlerComms(name:String, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array):void {
			registerVariableRead(name, new FunctionObject(callbackFunction, callbackTarget, callbackArgs));
		}
		
		private function getArrayKeysStartingWith(from:*, startingWith:String):Array {
			var variables:Array = new Array();
			for (var variableName:String in from) {
				if (variableName.indexOf(startingWith) == 0 && variableName.length > startingWith.length) {
					variables[variables.length] = variableName;
				}
			}
			return variables;
		}
		
		public function getSDTVariablesDenotedByObjectNotation(objectVariableName:String):Array {
			return getArrayKeysStartingWith(g.dialogueControl.advancedController._dialogueDataStore, objectVariableName);
		}
		
		public function getModVariablesDenotedByObjectNotation(objectVariableName:String):Array {
			return getArrayKeysStartingWith(registeredVariablesGet, objectVariableName);
		}
		
		public function getGlobalVariablesDenotedByObjectNotation(objectVariableName:String):Array {
			return getArrayKeysStartingWith(globalVariables, objectVariableName);
		}
		
		public function getLocalVariablesDenotedByObjectNotation(objectVariableName:String):Array {
			var denotedSDTVariables:Array = getSDTVariablesDenotedByObjectNotation(objectVariableName);
			var denotedModVariables:Array = getModVariablesDenotedByObjectNotation(objectVariableName);
			return denotedSDTVariables.concat(denotedModVariables);
		}
		
		public function hasGlobalVariable(variableName:String):Boolean {
			return (globalVariables[variableName] != null);
		}
		
		/**
		 * Sets the global variable. If attached, sets the local variable too. If caused by attachment, doesn't set local (infinite loop otherwise!)
		 * @param	variableName
		 * @param	value
		 * @param	causedByAttachment
		 */
		public function setGlobalVariableValue(variableName:String, value:*, causedByAttachment:Boolean = false):void {
			var globalVariable:GlobalVariable = globalVariables[variableName];
			if (globalVariable == null) {
				globalVariable = new GlobalVariable(value, false);
				globalVariables[variableName] = globalVariable;
			}
			globalVariable.setValue(value);
			if (!causedByAttachment && globalVariable.isAttached()) {
				setVariableValueViaSDT(variableName, value);
			}
		}
		
		public function registerGlobalVariable(variableName:String):void {
			//re-registering an existing variable should not give an error. Registering a non-existing variable SHOULD.
			copyLocalVariableToGlobal(variableName);
			linkGlobalVariable(variableName);
		}
		
		public function registerGlobalObjectVariable(objectVariableName:String):void {
			var variables:Array = getLocalVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				registerGlobalVariable(variables[i]);
			}
		}
		
		public function linkGlobalVariable(variableName:String):void {
			var globalVariable:GlobalVariable = globalVariables[variableName];
			if (globalVariable != null) {
				globalVariable.setAttached(true); //TODO check if value matches
			}
		}
		
		public function unlinkGlobalVariable(variableName:String):void {
			var globalVariable:GlobalVariable = globalVariables[variableName];
			if (globalVariable != null) {
				globalVariable.setAttached(false);
			}
		}
		
		public function unlinkAllGlobalVariables():void {
			for each (var globalVariable:GlobalVariable in globalVariables) {
				globalVariable.setAttached(false);
			}
		}
		
		public function unlinkGlobalObjectVariable(objectVariableName:String):void {
			var variables:Array = getGlobalVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				unlinkGlobalVariable(variables[i]);
			}
		}
		
		public function deleteLocalVariable(variableName:String):void {
			if (g.dialogueControl.advancedController._dialogueDataStore.hasOwnProperty(variableName)) {
				delete g.dialogueControl.advancedController._dialogueDataStore[variableName];
			}
		}
		
		public function deleteLocalObjectVariable(objectVariableName:String):void {
			var variables:Array = getSDTVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				deleteLocalVariable(variables[i]);
			}
		}
		
		public function deleteGlobalVariable(variableName:String):void {
			var globalVariable:GlobalVariable = globalVariables[variableName];
			if (globalVariable != null) {
				globalVariables.splice(globalVariables.indexOf(globalVariable), 1);
			}
		}
		
		public function deleteGlobalObjectVariable(objectVariableName:String):void {
			var variables:Array = getGlobalVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				deleteGlobalVariable(variables[i]);
			}
		}
		
		public function copyLocalVariableToGlobal(variableName:String):void {
			var value:* = getVariableValueViaSDT(variableName);
			setGlobalVariableValue(variableName, value);
		}
		
		public function copyLocalVariableToGlobalObject(objectVariableName:String):void {
			var variables:Array = getLocalVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				copyLocalVariableToGlobal(variables[i]);
			}
		}
		
		public function copyGlobalVariableToLocal(variableName:String):void {
			//get global variable, copy to local space
			var globalVariable:GlobalVariable = globalVariables[variableName];
			if (globalVariable != null) {
				var value:* = globalVariable.getValue();
				var result:* = setVariableValue(variableName, value);
				if (result != undefined) {
					return;
				}
				g.dialogueControl.advancedController._dialogueDataStore[variableName] = value; //Don't allow negative values to get parsed.
			}
		
		}
		
		public function copyGlobalVariableToLocalObject(objectVariableName:String):void {
			var variables:Array = getGlobalVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				copyGlobalVariableToLocal(variables[i]);
			}
		}
		
		public function loadGlobalVariable(variableName:String):void {
			copyGlobalVariableToLocal(variableName);
			linkGlobalVariable(variableName);
		}
		
		public function loadGlobalVariableObject(objectVariableName:String):void {
			var variables:Array = getGlobalVariablesDenotedByObjectNotation(objectVariableName);
			for (var i:uint = 0; i < variables.length; i++) {
				loadGlobalVariable(variables[i]);
			}
		}
		
		public function registerVariableWriteListener(name:String, callback:FunctionObject):void {
			if (writeListeners[name] == null) {
				writeListeners[name] = new Array();
			}
			writeListeners[name].push(callback);
		}
		
		public function registerVariableGetterSetter(name:String, getter:FunctionObject, setter:FunctionObject):void {
			registerVariableRead(name, getter);
			registerVariableWrite(name, setter);
		}
		
		public function registerVariableRead(name:String, callback:FunctionObject):void {
			if (!registeredVariablesGet.hasOwnProperty(name)) {
				registeredVariablesGet[name] = callback;
				dialogueLog("Variable getter for " + name + " registered.");
			}
		}
		
		public function registerVariableWrite(name:String, callback:FunctionObject):void {
			if (!registeredVariablesSet.hasOwnProperty(name)) {
				registeredVariablesSet[name] = callback;
				dialogueLog("Variable setter for " + name + " registered.");
			}
		}
		
		public function getVariableValue(name:String):* {
			if (registeredVariablesGet.hasOwnProperty(name)) {
				return registeredVariablesGet[name].call();
			}
		}
		
		/**
		 * 
		 * @param	name Variable name to set to
		 * @param	value value to set to the variable
		 * @return returns undefined if not in scope. Else, returns null or any return value given.
		 */
		public function setVariableValue(name:String, value:*):* {
			if (registeredVariablesSet.hasOwnProperty(name)) {
				var x:* = registeredVariablesSet[name].call(value);
				if (x == undefined) {
					x = null;
				}
				return x;
			}
			return undefined;
		}
		
		public function copyLocalObject(layers:Number, targetObjectVariable:String, sourceObjectVariable:String):void {
			var sourceVariables:Array = getLocalVariablesDenotedByObjectNotation(sourceObjectVariable);
			var periodCounter:uint = StringFunctions.countOccurances(sourceObjectVariable, ".");
			for (var i:uint = 0; i < sourceVariables.length; i++) {
				var variableName:String = sourceVariables[i];
				if (layers == 0 || (periodCounter + layers) >= StringFunctions.countOccurances(variableName, ".")) {
					var varValue:String = getVariableValueViaSDT(variableName);
					var varVal:Number = Number(varValue);
					if (isNaN(varVal)) {
						setVariableValueViaSDT(variableName.replace(sourceObjectVariable, targetObjectVariable), varValue);
					} else {
						setVariableValueViaSDT(variableName.replace(sourceObjectVariable, targetObjectVariable), varVal);
					}
				}
			}
		}
		
		public function copyLocalObjectUsingStructInfo(layers:Number, targetObjectVariable:String, sourceObjectVariable:String, structInfoObjectVariable:String):void {
			var sourceVariables:Array = getLocalVariablesDenotedByObjectNotation(sourceObjectVariable);
			var structVariables:Array = getLocalVariablesDenotedByObjectNotation(structInfoObjectVariable);
			var periodCounter:uint = StringFunctions.countOccurances(sourceObjectVariable, ".");
			for (var i:uint = 0; i < sourceVariables.length; i++) {
				var variableName:String = sourceVariables[i];
				if ((layers == 0 || (periodCounter + layers) >= StringFunctions.countOccurances(variableName, ".")) && structVariables.indexOf(variableName.replace(sourceObjectVariable, structInfoObjectVariable)) != -1) {
					var varValue:String = getVariableValueViaSDT(variableName);
					var varVal:Number = Number(varValue);
					if (isNaN(varVal)) {
						setVariableValueViaSDT(variableName.replace(sourceObjectVariable, targetObjectVariable), varValue);
					} else {
						setVariableValueViaSDT(variableName.replace(sourceObjectVariable, targetObjectVariable), varVal);
					}
				}
			}
		}
		
		public function variableInsert(args:Array) { //No return type, might cause problems
			if (args.length != 4) {
				return;
			}
			var x:* = getVariableValue(args[1]);
			if (x != undefined) {
				return x;
			} else {
				var index:int = findIndexOfNextSplit(args[1]);
				if (index != -1) {
					return evaluateStringQuation(args[1]);
				}
			}
		}
		
		public function evaluateStringQuation(str:String):Number {
			var arr:Array = str.split(" ");
			for (var i:uint = 0, isize:uint = arr.length; i < isize; i++) {
				var variableName:String = arr[i];
				var value:String = getVariableValueViaSDT(variableName);
				if (value != "") {
					arr[i] = value;
				}
			}
			var variable:String = arr.join(" ");
			return StringQuation.equationMath(variable);
		}
		
		public function getVariableValueViaSDT(name:String):String {
			var index:int = findIndexOfNextSplit(name);
			if (index == -2) { //variable IS an operator
				return name;
			}
			if (index != -1) {
				throw new Error("Dialogue variable has operator in name - infinite loop - operator is (" + name.charAt(index) + "), full name is: " + name);
				return "";
			}
			return g.dialogueControl.advancedController.replaceValues("*" + name + "*");
		}
		
		public function getVariableValueViaSDTRaw(name:String):* {
			return g.dialogueControl.advancedController.replaceValues("*" + name + "*");
		}
		
		public function setVariableValueViaSDT(name:String, value:*):void {
			var o:Object = new Object();
			o[name] = value;
			//dialogueLog("setVariableValueViaSDT set " + name + " : " + value);
			g.dialogueControl.advancedController.setValues(o);
		}
		
		//Replaces SDT's g.dialogueControl.advancedController.setValues, used for setting values in the "set" line-attribute.
		public function variableWrite(variables:*) {
			var varname:* = null;
			var value:* = null;
			
			for (varname in variables) {
				var copyValue:Boolean = false;
				//g.dialogueControl.advancedController.outputLog("Variable " + varname + " found");
				//g.dialogueControl.advancedController.outputLog("rest" + variables);
				value = variables[varname];
				var result:* = setVariableValue(varname, value);//check for handlers
				if (result !== undefined) {
					continue;
				}
				if (hasGlobalVariable(varname)) {
					copyValue = globalVariables[varname].isAttached();
				}
				var oldValue:* = null;
				if (g.dialogueControl.advancedController._dialogueDataStore.hasOwnProperty(varname)) {
					oldValue = g.dialogueControl.advancedController._dialogueDataStore[varname];
				}
				if (value is String && !isNaN(Number(value)) && value != " ") {
					if (!g.dialogueControl.advancedController._dialogueDataStore.hasOwnProperty(varname)) {
						g.dialogueControl.advancedController._dialogueDataStore[varname] = 0;
					}
					if ((value as String).charAt(0) == "-") {
						g.dialogueControl.advancedController._dialogueDataStore[varname] = g.dialogueControl.advancedController._dialogueDataStore[varname] + Number(value);
							// g.dialogueControl.advancedController.outputLog(varname + ": " + "decrement by " + Number(value) + " = " + g.dialogueControl.advancedController._dialogueDataStore[varname]);
					} else if ((value as String).charAt(0) == "+") {
						g.dialogueControl.advancedController._dialogueDataStore[varname] = g.dialogueControl.advancedController._dialogueDataStore[varname] + Number(value);
							//g.dialogueControl.advancedController.outputLog(varname + ": " + "increment by " + Number(value) + " = " + g.dialogueControl.advancedController._dialogueDataStore[varname]);
					}
				} else {
					g.dialogueControl.advancedController._dialogueDataStore[varname] = value;
				}
				//g.dialogueControl.advancedController.outputLog("Storing " + varname + " = " + value);
				if (copyValue) {
					setGlobalVariableValue(varname, g.dialogueControl.advancedController._dialogueDataStore[varname], true);
				}
				if (writeListeners[varname] != null) { //notify listeners
					for each (var listener in writeListeners[varname]) {
						listener.call(oldValue, g.dialogueControl.advancedController._dialogueDataStore[varname]);
					}
				}
			}
			return;
		}
		
		public function findIndexOfNextSplit(variable:String):int {
			var operators:Array = ["+", "-", "*", "%", "/", "\\", "(", ")", "==", "!=", "<=", ">=", "||", "&&", ">", "<", "="];
			if (operators.indexOf(variable) != -1) {
				return -2; //Nope, not an operator, truuuust me. Hotfix to prevent false positive errors.
			}
			
			var checkArray:Array = new Array();
			checkArray.push([variable.indexOf("+"), "+"]);
			checkArray.push([variable.indexOf("-"), "-"]);
			checkArray.push([variable.indexOf("*"), "*"]);
			checkArray.push([variable.indexOf("%"), "%"]);
			checkArray.push([variable.indexOf("/"), "/"]);
			checkArray.push([variable.indexOf("\\"), "\\"]);
			checkArray.push([variable.indexOf("("), "("]);
			checkArray.push([variable.indexOf(")"), ")"]);
			checkArray.push([variable.indexOf("=="), "=="]);
			checkArray.push([variable.indexOf("!="), "!="]);
			checkArray.push([variable.indexOf("<="), "<="]);
			checkArray.push([variable.indexOf(">="), ">="]);
			checkArray.push([variable.indexOf("||"), "||"]);
			checkArray.push([variable.indexOf("&&"), "&&"]);
			checkArray.push([variable.indexOf(">"), ">"]); //Yes, I could have skipped <= and >=, but I didn't feel like it.
			checkArray.push([variable.indexOf("<"), "<"]);
			checkArray.push([variable.indexOf("="), "="]);
			checkArray.sortOn([0], [Array.NUMERIC]); //And voila.
			for (var i:uint = 0, isize:uint = checkArray.length; i < isize; i++) {
				if (checkArray[i][0] != -1) {
					return checkArray[i][0];
				}
			}
			return -1;
		}
		
		public function dialogueLog(message:String):void {
			g.dialogueControl.advancedController.outputLog("DialogueActions(VariableManager): "+message);
		}
	
	}

}