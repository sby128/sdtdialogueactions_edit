package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard03_2.swf",symbol="WWSound_AhHard03_2")]
	
	public dynamic class WWSound_AhHard03_2 extends Sound {
		
		public function WWSound_AhHard03_2() {
			super();
		}
	}

}
