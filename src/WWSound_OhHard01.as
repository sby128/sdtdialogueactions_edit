package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard01.swf",symbol="WWSound_OhHard01")]
	
	public dynamic class WWSound_OhHard01 extends Sound {
		
		public function WWSound_OhHard01() {
			super();
		}
	}

}
