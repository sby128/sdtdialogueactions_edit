package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft02.swf",symbol="WWSound_OhSoft02")]
	
	public dynamic class WWSound_OhSoft02 extends Sound {
		
		public function WWSound_OhSoft02() {
			super();
		}
	}

}
