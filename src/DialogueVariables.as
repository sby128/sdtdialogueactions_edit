package  
{
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.IOErrorEvent;
	import flash.events.Event;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class DialogueVariables extends CustomVariableHolder
	{
		private var dialogueToLoad:String = "";
		public function DialogueVariables(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableWrite("da.dialogue.load", new FunctionObject(addLineChangeListener, this, []));
			v.registerVariableWrite("da.dialogue.import", new FunctionObject(importDialogue, this, []));
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("CLEARLINES", 1, new FunctionObject(clearLinesFor, this, []));
		}
		
		public function clearLinesFor(lineType:String):void {
			//TODO find out why this works for SDT but crashes for me
			
			/*
			   if (allDialogueTypes.indexOf(UnknownSlot) != -1 && !this.customClears[UnknownSlot])
               {
                  this.customClears[UnknownSlot] = true;
               }
               if (!this.customLibrary[UnknownSlot])
               {
                  this.customLibrary[UnknownSlot] = new Array();
               } 
			 */
			
			if (/*g.dialogueControl.library.allDialogueTypes.indexOf(lineType) != -1 &&*/ !g.dialogueControl.library.customClears[lineType]) {
				g.dialogueControl.library.customClears[lineType] = true;
			}
			g.dialogueControl.library.customLibrary[lineType] = new Array();
			
		}
		
		public function importDialogue(dialoguePath:String):void {
			var fullPath:String = m.getFileReferenceHandler().convertFilePath(dialoguePath);
			DialogueLoader.loadDialogueFile(fullPath, new FunctionObject(this.onDialogueImport, this, []), new FunctionObject(this.dialogueLoadFail, this, []));
		}
		
		//Based on g.dialogueControl.library.loadCustomDialogue
		public function onDialogueImport(event:*):void {
			var dialogueAsString:String = event.target.data;
			if (dialogueAsString) {
				g.dialogueControl.library.outputLog("Importing custom dialogue.\n------------------------------");
			 
				dialogueAsString.replace(/vigourous:/g,"vigorous:");
				var dialogueNameRegExp:RegExp = new RegExp("dialogue_name" + ":\\s?\"(.+?)\"");
				var regExpMatch = dialogueAsString.match(dialogueNameRegExp);
				if (regExpMatch && regExpMatch.length > 0)
				{
				   regExpMatch[0].replace(dialogueNameRegExp, "$1");//this normally sets the dialogue name, but I get the feeling this strips the line too so I'm leaving it in
				}
			
				var initialSettingsRegExp = new RegExp("initial_settings" + ":(.*)");
				regExpMatch = dialogueAsString.match(initialSettingsRegExp);
				if (regExpMatch && regExpMatch.length == 2)
				{
				   dialogueAsString = dialogueAsString.replace(initialSettingsRegExp,"");// remove initial settings line
				   /*this.outputLog("Reading initial settings.");
				   try
				   {
					  this.initialSettingsObject = com.adobe.serialization.json.JSON.decode(UnknownSlot[1],false);
					  this.outputLog("   Okay.");
					  g.dialogueControl.advancedController.setValues(this.initialSettingsObject);
				   }
				   catch(e:Error)
				   {
					  initialSettingsObject = {};
					  outputLog("ERROR - Could not decode initial settings:\n   " + UnknownSlot[1] + "\n   " + e.message);
				   }*/
				}
				regExpMatch = dialogueAsString.match(/all:\s?"CLEAR"/);
				if (regExpMatch && regExpMatch.length > 0)
				{
				   g.dialogueControl.library.allCleared = true;
				}
				regExpMatch = dialogueAsString.match(/finish:\s?"CLEAR"/);
				if (regExpMatch && regExpMatch.length > 0)
				{
				   g.dialogueControl.library.finishesCleared = true;
				}
				dialogueAsString = dialogueAsString.replace(/finish(.+):"(.*)"/g,g.dialogueControl.library.customFinishPopulator);
				dialogueAsString.replace(/(.*?):"(.*?)"(.*)/g, g.dialogueControl.library.customLibraryPopulator);
				g.dialogueControl.library.outputLog("Done.\n------------------------------");
			}
		}
		
		public function addLineChangeListener(dialoguepath:String):void {
			dialogueToLoad = dialoguepath;
			m.addLineChangeListener(new FunctionObject(onLineChange, this, []));
		}
		
		public function onLineChange(...args):void {
			loadDialogue(dialogueToLoad);
		}
		
		public function loadDialogue(path:String):void {
			var fullPath:String = m.getFileReferenceHandler().convertFilePath(path);
			DialogueLoader.loadDialogueFile(fullPath, new FunctionObject(this.onDialogueLoad, this, []), new FunctionObject(this.dialogueLoadFail, this, []));
		}
		
		public function onDialogueLoad(event:*):void {
			var dialogueAsString:String = event.target.data;
			g.dialogueControl.loadCustomDialogue(dialogueAsString);
		}
		
		public function dialogueLoadFail(path:String, e:Event = null):void {
			m.displayMessageRed("Couldn't find Dialogue file: " + path);
		}
	}

}