package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft03.swf",symbol="WWSound_OhSoft03")]
	
	public dynamic class WWSound_OhSoft03 extends Sound {
		
		public function WWSound_OhSoft03() {
			super();
		}
	}

}
