package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard01_2.swf",symbol="WWSound_OhHard01_2")]
	
	public dynamic class WWSound_OhHard01_2 extends Sound {
		
		public function WWSound_OhHard01_2() {
			super();
		}
	}

}
