Variable are scoped to namespaces in Dialogues.

There are four namespaces:
- local, which is the normal namespace of a dialogue variable. Vanilla SDT only has local variables.
- global, which is the namespace DialogueActions creates and uses for SETGLOBAL and related triggers.
- save, which is the namespace DialogueActions uses for saved variables (saved with SAVESETVARS).
- mod, which is the namespace certain variables of DialogueActions use. Mod variables generally have certain restrictions to them (such as read-only).

Local namespace
The local namespace is the namespace that's accessible to you in a dialogue via conventional means.
The local namespace contains variables you create in initial_settings and set lineattributes.
Variables in the local namespace disappear whenever a new dialogue is loaded.

Global namespace
The global namespace is the namespace used by DialogueActions to store variables declared as global.
You cannot directly access global variables in a dialogue.
Global and local variables can be linked together via triggers such as REGISTERGLOBALS and LOADGLOBALS.
To link a variable between the local and global namespaces, the variable needs to have the same name in both namespaces.
Prior to linking a variable, the values of the variable in the namespaces must be the same.
REGISTERGLOBALS will set both values to be the local variable's value.
LOADGLOBALS will set both values to be the global variable's value.
When a new dialogue is loaded, local variables will disappear.
Global variables will still exist. You can thus use global variables to send variable values between dialogues.
Like this, you can separate a dialogue into chapters without having to create complex structures.
Beware: If a linked variable is deleted, and a new local variable is created with the same name, then the value of the global variable will be overwritten with the local variable's value.
To prevent this, unlink your variables before loading a new dialogue, or don't declare copied variables in your initial_settings line.
Global variables will disappear when SDT is closed.

Save namespace
The save namespace is the namespace used by DialogueActions to save variables to a savefile.
You cannot directly access save variables in a dialogue.
Via the use of SAVESETVARS and SAVEGETVARS you can write and read from the save namespace.
Save variables will not disappear when SDT is closed.
They can get deleted by various other programs though.

Mod namespace
Mod namespace is the namespace that variables from mods reside in.
These variables don't exist in the local namespace, but you can still directly access them.
The mods intercept the variable read/writes and give the proper values back.
That said, you shouldn't link mod variables with the global namespace.
Bad things might happen.
Most likely you'll find the game slowing down because it has to update dialogue variables each frame...
Or you'll find your global variables to be out-of-sync with the mod variables.
Or your clothes can't change colors anymore.
Just don't register mod variables as globals.