Place SDTDialogueActionsv<VERSION>.swf in your $INIT$ or character folder.
Add the file's name to Mods.txt in that folder.
Note: Since v1.09, support for loading external files from a character folder different than the location of DialogueActions has been added.
So you should just install it in $INIT$, unless there's something wrong with DialogueActions (hah, like if Konashion came back and everything went incompatible)
(For the record, SDT v1.21.1b)